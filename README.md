fensterchen
===========

Framework for exercise 02.

Add your classes to the `framework/` directory and extend `source/tests.cpp`
with your test cases.

#Glossar

* **Klasse:**
Man kann die Klasse als einen Bauplan oder abstraktes Modell eines Objekts betrachten. Hier werden Attribute/Eigenschaften und Methoden/Verhalten fest gelegt, die ein Objekt dieser Klasse innehaben soll.

* **Objekt:**
Objekte sind konkrete Instanzen einer Klasse die während der Laufzeit erzeugt werden können. Während ihrer Existenz besitzen Objekte einen bestimmten Zustand, der von seinen Eigenschaften definiert wird, und ein Verhalten, welches von seinen Methoden bestimmt wird.

* **Instanziierung:**
Die Instanziierung bezeichnet das Erzeugen eines Objekts. Dabei wird der Konstuktor einer Klasse ausgeführt und es werden Resourcen im Speicher belegt.

* **Instanz:**
Siehe "Objekt".

* **Statische Objekterzeugung:**
Wird ein Objekt statisch erzeugt, so werden ihm Resourcen auf dem Stack zugeordnet. Der Stack ist ein FIFO-Speicher. D.h. was zuerst gespeichert wird, wird auch zuletzt wieder frei gegeben. Ein statisch erzeugtes Objekt existiert so lange wie die Funktion, in der es erzeugt wurde. D.h. ist ein Objekt erst einmal statisch erzeugt, so hat man keinen direkten Einlfuss mehr auf seine Zerstörung weil die Speicherverwaltung automatisch erfolg.

    * :heavy_plus_sign: Schnellere Zugriffszeiten
    * :heavy_plus_sign: Speicherzuordnung erfolgt automatisch
    * :heavy_plus_sign: Speicher wird effizient verwaltet, keine Fragmentierung
    * :heavy_minus_sign: nur lokaler Zugriff
    * :heavy_minus_sign: begrenzte Speichergröße
    * :heavy_minus_sign: Größe der Objekte kann nicht geändert werden


* **Dynamische Objekterzeugung:**
Wird ein Objekt dynamisch erzeugt, so werden ihm Resourcen auf dem Free Store oder Heap zugeordnet. Dynamische Erzeugung funktioniert meist über Zeiger, die die Adresse des erzeugten Objekts speichern. Objekte auf dem Heap können zu einem beliebigen Zeitpunkt in der Laufzeit wieder gelöscht werden. Weil die Speicherverwaltung auf dem Heap nicht automatisch erfolgt wie auf dem Stack, muss sogar darauf geachtet werden, dass der Speicher, der für ein Objekt bestimmt war, explizit wieder frei gegeben wird. Ansonsten erzeugt man Memory Leaks - Speicherbereiche auf die nicht zugegriffen werden kann.

    * :heavy_plus_sign: Variablen können global erreicht werden
    * :heavy_plus_sign: mehr Speicher als auf dem Stack
    * :heavy_plus_sign: Objekte können wachsen oder schrumpfen
    * :heavy_plus_sign: Speicher wird nicht effizient genutzt und kann fragmentieren
    * :heavy_minus_sign: Speicher muss manuell vom Programmierer verwaltet werden
    * :heavy_minus_sign: relativ langsame Zugriffszeiten

* **Methode:**
Methoden sind quasi kleinere Unterprogramme, die Teile von größeren Problemen lösen sollen oder das Verhalten eines Objekts definieren.

* **Getter:**
Sie gewähren Lesezugriff auf Attribute eines Objekts, die wegen Kapselung nicht verfügbar wären.

* **Setter:**
Setter sind das Gegenteil der Getter. Sie gewähren Schreibzugriff auf abgekapselte Attribute eines Objekts.

* **Kapselung:**
In einer Klasse können Attribute und Methoden mit Hilfe einer Schnittstelle (Getter, Setter) von außen verfügbar gemacht oder versteckt werden. Damit gibt es die Möglichkeit, den Zugriff auf interne Implementierungsdetails besser zu steuern. Außerdem kann hiermit garantiert werden, dass der Zustand eines Objekts nicht versehentlich geändert werden kann.

* **public:**
public ist ein Schlüsselwort der OOP. Es wird verwendet um in einer Klassendefinition den öffentlichen Block anzugeben. Hier werden Attribute und Mehtoden untergebracht auf die Vollzugriff gewährt werden soll.

* **private:**
Mit private wird der Private Block einer Klassendefinition angegeben. Hier werden Attribute und Methoden untergebracht, die nicht oder nur über ein Interface zugänglich sein sollen.

* **C++ Präprozessor:**
Der Präprozessor wird vor der eigentlichen Übersetzung eine Programms aufgerufen und bearbeitet folgende Aufgaben:

    - Einfügen von Header Dateien
    - Definieren von Konstanten über #define
    - bedingte Kompilierung durch #ifndef etc.
    - gibt es hier schon Fehler kann der Präprozessor schon vor der Übersetzung Fehlermeldungen ausgeben.

* **Konstruktor:**
Beim Erzeugen eines Objekts, wird sein Konstruktor aufgerufen. Er sorgt dafür, dass Speicher verbestimmt wird und dass sämtliche Attribute instanziiert werden.

* **Standardkonstruktor/Default-Constructor:**
Der Standardkonstruktor wird ohne Parameter aufgerufen. Allerdings kann er trotzdem eine Initialisierungsliste besitzen. Wird in der Klassendefinition kein Standardkonstruktor angegeben, so wird er vom Compiler synthtisiert.

* **Destruktor:**
Der Destruktor wird automatisch aufgerufen wenn ein Objekt zerstört wird. Wenn spezielle Aufräumarbeiten erledigt werden müssen, sollte man diese explizit in den Destruktor schreiben.

* **Membervariablen:**
Membervariablen sind die Attribute eines Objekts.

* **Klassendeklaration:**
In der Klassendeklaration werden sämliche Methoden und Attribute einer Klasse aufgelistet (deklariert).

* **Initialisierungsliste:**
In der Initialisierungsliste werden innerhalb eines Konstruktors sämtliche Membervariablen initialisiert. Dies spart im Gegensatz zum alten C-Style einen Arbeitsschritt, nämlich eine Variable zu erzeugen, sie zu Copyconstructen und dann am Ende des Konstuktors zu zerstören.

* **class:**
Schlüsselwort für den Start einer Klassendeklaration.

* **struct:**
Schlüsselwort für den Start einer Structdeklaration. Structs unterscheiden sich von Klassen nur in einer Hinsicht, nämlich: ohne Angabe einer Kapselung sind sie standardmäßig public.

* **Datentransferobjekt:**
Objekte die keine nennenswerten Memberfunktionen oder Dienste anbieten und alle ihre Attribute öffentlich genutzt werden können, sind Datentransferobjekte.

* **Statische Typisierung:**
In Programmiersprachen mit statischer Typisierung muss eine Variable vor der Benutzung zumindest deklariert werden.

* **Dynamische Typisierung:**
In Programmiersprachen mit dynamischer Typisierung können Variablen während der Laufzeit erzeugt werden.

* **Framework:**
Stellt ein Programmgerüst dar, dass von einem Programmierer genutzt werden kann, damit er gängige Problemlösungen nicht neu implementieren muss.