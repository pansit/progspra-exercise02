#include "circle.hpp"

Circle::Circle()
	: Shape{Point2d{}, Color{0.0}}
	, radius_{0.0}
{}

Circle::Circle(Point2d const& location, double const radius, Color const& color)
	: Shape{location, color}
	, radius_{radius}
{}

Circle::~Circle(){}

double Circle::radius() const {
	return radius_;
}

double Circle::circumference() const {
	return 2 * M_PI * radius_;
}

void Circle::draw(Window const& w) const {
	for (int i = 0; i < segments_*2; ++i) {
		w.drawLine(
			location_.x() + (radius_ * std::cos((i * M_PI)/segments_)),
			location_.y() + (radius_ * std::sin((i * M_PI)/segments_)),
			location_.x() + (radius_ * std::cos(((i+1) * M_PI)/segments_)),
			location_.y() + (radius_ * std::sin(((i+1) * M_PI)/segments_)),
			color_.r,
			color_.g,
			color_.b
		);
	}
}

void Circle::draw(Window const& w, Color const& color) const {
	for (int i = 0; i < segments_*2; ++i) {
		w.drawLine(
			location_.x() + (radius_ * std::cos((i * M_PI)/segments_)),
			location_.y() + (radius_ * std::sin((i * M_PI)/segments_)),
			location_.x() + (radius_ * std::cos(((i+1) * M_PI)/segments_)),
			location_.y() + (radius_ * std::sin(((i+1) * M_PI)/segments_)),
			color.r,
			color.g,
			color.b
		);
	}
}

bool Circle::is_inside(Point2d const& point) const {
	return std::sqrt(
		pow(location_.x() - point.x(),2) +
		pow(location_.y() - point.y(),2)
	) < radius_;
}