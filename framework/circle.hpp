#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "fensterchen.hpp"

class Circle : public Shape
{
public:
	Circle();
	Circle(Point2d const& location, double const radius, Color const& color);
	~Circle();

	double radius() const;
	double circumference() const;
	void draw(Window const& w) const;
	void draw(Window const& w, Color const& color) const;
	bool is_inside(Point2d const& point) const;

private:
	double radius_;

	// Hälfte der Segmente mit denen der Kreis gezeichnet wird.
	unsigned char segments_ = 60;
};

#endif // CIRCLE_HPP