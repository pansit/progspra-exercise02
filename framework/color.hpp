#ifndef COLOR_HPP
#define COLOR_HPP

struct Color
{
	Color(float value)
		: r{value}
		, g{value}
		, b{value}
	{}

	Color(float r, float g, float b)
		: r{r}
		, g{g}
		, b{b}
	{}

	float r;
	float g;
	float b;
};

#endif // COLOR_HPP