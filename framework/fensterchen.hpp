#ifndef BUW_FENSTERCHEN_HPP
#define BUW_FENSTERCHEN_HPP

#include "window.hpp"
#include "point2d.hpp"
#include "color.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "rectangle.hpp"

#include <memory>

#endif // BUW_FENSTERCHEN_HPP
