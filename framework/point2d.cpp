#include "point2d.hpp"
#include <cmath>

Point2d::Point2d()
	: x_{0.0}
	, y_{0.0}
{}

Point2d::Point2d(double x, double y)
	: x_{x}
	, y_{y}
{}

Point2d::~Point2d(){}

bool operator== (Point2d const& p1, Point2d const& p2) {
	return p1.x() == p2.x() && p1.y() == p2.y();
}

bool operator!= (Point2d const& p1, Point2d const& p2) {
	return !(p1 == p2);
}

/**
 * Getter Method
 * @return x coordinate of the point
 */
double Point2d::x() const {
	return x_;
}

/**
 * Getter Method
 * @return y coordinate of the point
 */
double Point2d::y() const {
	return y_;
}

/**
 * Translates the point the given values
 * @param x Translates in x-direction
 * @param y Translates in y-direction
 */
void Point2d::translate(double const x, double const y) {
	x_ += x;
	y_ += y;
}

/**
 * This function rotates the point around the origin.
 * @param value The function accepts an angle as radian measure.
 */
void Point2d::rotate(double const radians) {
	double x = x_;
	double y = y_;
	x_ = x * std::cos(radians) - y * std::sin(radians);
	y_ = x * std::sin(radians) + y * std::cos(radians);
}

void Point2d::rotate(double const radians, Point2d const& center) {
	double x = x_;
	double y = y_;
	x_ = center.x() + (x - center.x()) * std::cos(radians) -
			(y - center.y()) * std::sin(radians);
	y_ = center.y() + (x - center.x()) * std::sin(radians) +
			(y - center.y()) * std::cos(radians);
}

/**
 * This function rotates the point around a given pivot point.
 * @param radians  Angle of the rotation in radian measure
 * @param p      Pivot point
 * @param radius Distance between the two points
 */
void Point2d::rotate(double radians, Point2d const& center, double radius) {
	radians -= M_PI/4.0;
	radius = sqrt(pow(radius,2)/2.0);
	x_ = center.x() + radius * (std::cos(radians) - std::sin(radians));
	y_ = center.y() + radius * (std::sin(radians) + std::cos(radians));
}
