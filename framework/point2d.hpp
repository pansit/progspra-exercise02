// Include guards verhindern die mehrfache Einbindung von header-Dateien.
#ifndef BUW_POINT2D_HPP
#define BUW_POINT2D_HPP

class Point2d
{
public:
	/**
	 * Standardkonstruktor
	 */
	Point2d();

	/**
	 * Benutzerdefinierter Konstruktor
	 */
	Point2d(double x, double y);

	/**
	 * Destruktor:
	 * Der Destruktor wird aufgerufen wenn der Gültigkeitsbereich eines Objektes
	 * endet. Es ist besonders dann wichtig, wenn ein Objekt Ressourcen
	 * angefordert hat. Diese können vom Destruktor explizit wieder frei gegeben
	 * werden.
	 */
	~Point2d();

	friend bool operator== (Point2d const& p1, Point2d const& p2);
	friend bool operator!= (Point2d const& p1, Point2d const& p2);

	double x() const;
	double y() const;
	void translate(double const x, double const y);
	void rotate(double const radians);
	void rotate(double const radians, Point2d const& center);
	void rotate(double radians, Point2d const& center, double const radius);

private:
	double x_;
	double y_;
};

#endif // BUW_POINT2D_HPP