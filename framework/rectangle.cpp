#include "rectangle.hpp"

Rectangle::Rectangle()
	: Shape{Point2d{}, Color{0.0}}
	, width_{0}
	, height_{0}
{}

Rectangle::Rectangle(Point2d const& location, double const width,
										 double const height, Color const& color)
	: Shape{location, color}
	, width_{width}
	, height_{height}
{}

Rectangle::~Rectangle(){}

double Rectangle::width() const {
	return width_;
}

double Rectangle::height() const {
	return height_;
}

double Rectangle::circumference() const {
	return 2 * width_ + 2 * height_;
}

void Rectangle::draw(Window const& w) const {
	Point2d p{location_.x()+width_, location_.y()+height_};
	w.drawLine(
		location_.x(), location_.y(),
		location_.x(), p.y(),
		color_.r, color_.g, color_.b
	);
	w.drawLine(
		p.x(), location_.y(),
		p.x(), p.y(),
		color_.r, color_.g, color_.b
	);
	w.drawLine(
		location_.x(), location_.y(),
		p.x(), location_.y(),
		color_.r, color_.g, color_.b
	);
	w.drawLine(
		location_.x(), p.y(),
		p.x(), p.y(),
		color_.r, color_.g, color_.b
	);
}

void Rectangle::draw(Window const& w, Color const& clr) const {
	Point2d p{location_.x()+width_, location_.y()+height_};
	w.drawLine(
		location_.x(), location_.y(),
		location_.x(), p.y(),
		clr.r, clr.g, clr.b
	);
	w.drawLine(
		p.x(), location_.y(),
		p.x(), p.y(),
		clr.r, clr.g, clr.b
	);
	w.drawLine(
		location_.x(), location_.y(),
		p.x(), location_.y(),
		clr.r, clr.g, clr.b
	);
	w.drawLine(
		location_.x(), p.y(),
		p.x(), p.y(),
		clr.r, clr.g, clr.b
	);
}

bool Rectangle::is_inside(Point2d const& point) const {
	return !(
		(point.x() < location_.x()) ||
		(point.y() < location_.y()) ||
		(point.x() > location_.x() + width_) ||
		(point.y() > location_.y() + height_)
	);
}