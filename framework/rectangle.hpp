#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "fensterchen.hpp"

class Rectangle : public Shape
{
public:
	Rectangle();
	Rectangle(Point2d const& location, double const width, double const height,
						Color const& color);
	~Rectangle();

	double width() const;
	double height() const;
	double circumference() const;
	void draw(Window const& w) const;
	void draw(Window const& w, Color const& clr) const;
	bool is_inside(Point2d const& point) const;

private:
	double width_;
	double height_;
};

#endif // RECTANGLE_HPP