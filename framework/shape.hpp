#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "fensterchen.hpp"

class Shape
{
public:
  Shape()
    : location_{Point2d{}}
    , color_{Color{0}}
  {}

  Shape(Point2d const& location, Color const& color)
    : location_{location}
    , color_{color}
  {}

  ~Shape(){}

  Point2d location() const {
    return location_;
  }

  Color color() const {
    return color_;
  }

  void color(float const r, float const g, float const b) {
    color_.r = r;
    color_.g = g;
    color_.b = b;
  }

  void translate(double const x, double const y) {
    location_.translate(x,y);
  }

  void rotate(double radians, Point2d const& p) {
    location_.rotate(radians, p);
  }

  virtual void draw(Window const&) const = 0;
  virtual void draw(Window const&, Color const&) const = 0;
  virtual bool is_inside(Point2d const& point) const = 0;

protected:
  Point2d location_;
  Color color_;
};

#endif // SHAPE_HPP