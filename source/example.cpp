#include <fensterchen.hpp>
#include <vector>
#include <iostream>

int main(int argc, char* argv[])
{
  std::vector<std::shared_ptr<Shape>> v;
  Point2d helper;
  for (double i = 0; i < 1; i += 1/5.0) {
    helper.rotate(M_PI*2*i,Point2d{.5,.5}, .2);

    v.push_back(std::make_shared<Circle>(
      Point2d{helper.x()-.2,helper.y()},
      .05,
      Color{1-(float)i,(float)i,0}
    ));
    v.push_back(std::make_shared<Circle>(
      Point2d{helper.x()+.2,helper.y()},
      .05,
      Color{1-(float)i,(float)i,0}
    ));

  }

  Window win(glm::ivec2(800,800));
  Point2d p1{.3,.5};
  Point2d p2{.7,.5};
  Circle c1{p1,.1,Color{0,1,0}};
  Rectangle r1{p2,.2,.2,Color{0,1,0}};
  r1.translate(-.1,-.1);
  while (!win.shouldClose()) {
    if (win.isKeyPressed(GLFW_KEY_ESCAPE)) {
      win.stop();
    }

    auto t = win.getTime();

    Point2d mouse{win.mousePosition().x,win.mousePosition().y};

    if (c1.is_inside(mouse)) c1.draw(win, Color{0,0,1});
    else c1.draw(win);
    if (r1.is_inside(mouse)) r1.draw(win, Color{0,0,1});
    else r1.draw(win);

    bool toggle = true;
    for (auto i : v) {
      if (toggle) {
        i->rotate(0.03, p1);
        toggle = false;
      } else {
        i->rotate(-0.03, p2);
        toggle = true;
      }
      if(i->is_inside(mouse))
        i->draw(win, Color{0,0,1});
      else
        i->draw(win);
    }

    win.update();
  }

  return 0;
}
