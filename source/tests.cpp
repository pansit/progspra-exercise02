#define CATCH_CONFIG_RUNNER
#include "point2d.hpp"
#include "color.hpp"
#include "circle.hpp"
#include "rectangle.hpp"
#include <catch.hpp>

// POINT2D CONSTRUCTOR
TEST_CASE("create_point_origin", "[create_point_origin]")
{
	Point2d p;
	REQUIRE(p.x() == 0);
	REQUIRE(p.y() == 0);
}

TEST_CASE("create_point_1", "[create_point_1]")
{
	Point2d p{2.0,5.123};
  REQUIRE(p.x() == 2.0);
  REQUIRE(p.y() == Approx(5.123));
  REQUIRE(p.y() > 0.0);
}

TEST_CASE("create_point_2", "[create_point_2]")
{
	Point2d p{2.1123,-0.0001};
	REQUIRE(p.x() == Approx(2.1123));
	REQUIRE(p.y() == Approx(-0.0001));
}

TEST_CASE("create_point_3", "[create_point_3]")
{
	Point2d p{-0,-0.0001};
	REQUIRE(p.x() == Approx(-0));
	REQUIRE(p.y() == Approx(-0.0001));
}

TEST_CASE("create_point_4", "[create_point_4]")
{
	Point2d p{-2.1123,0.0001};
	REQUIRE(p.x() == Approx(-2.1123));
	REQUIRE(p.y() == Approx(0.0001));
}

// TRANSLATE
TEST_CASE("translate_point_1", "[translate_point_1]")
{
	Point2d p{5,5};
	p.translate(-5,-5);
	REQUIRE(p.x() == 0);
	REQUIRE(p.y() == 0);
}

TEST_CASE("translate_point_2", "[translate_point_2]")
{
	Point2d p{5.97,5.53};
	p.translate(-5,-5);
	REQUIRE(p.x() == Approx(0.97));
	REQUIRE(p.y() == Approx(0.53));
}

// ROTATE
TEST_CASE("rotate_point_1", "[rotate_point_1]")
{
	Point2d p{1.0,0.0};
	p.rotate(M_PI);
	REQUIRE(p.x() == Approx(-1.0));
	REQUIRE(p.y() == Approx(0.0));
}

TEST_CASE("rotate_point_2", "[rotate_point_2]")
{
	Point2d p{1,0};
	p.rotate(M_PI/4.0);
	REQUIRE(p.x() == Approx(sqrt(2)/2));
	REQUIRE(p.y() == Approx(sqrt(2)/2));
}

TEST_CASE("rotate_point_3", "[rotate_around_any_point_A]")
{
	Point2d p;
	p.rotate(M_PI/2.0, Point2d{1,0}, 1.5);
	REQUIRE(p.x() == Approx(1.0));
}

TEST_CASE("rotate_point_4", "[rotate_around_any_point_B]")
{
	Point2d p;
	p.rotate(M_PI/2.0, Point2d{1,0}, 1.5);
	REQUIRE(p.y() == Approx(1.5));
}

TEST_CASE("rotate_point_5", "[rotate_around_another_point_A]")
{
	Point2d p1{1,1};
	Point2d p2{2,1};
	p2.rotate(M_PI/4.0, p1);
	REQUIRE(p2.x() == Approx(1.7071067812));
	REQUIRE(p2.y() == Approx(1.7071067812));
	p2.rotate(M_PI, p1);
	REQUIRE(p2.x() == Approx(0.2928932188));
	REQUIRE(p2.y() == Approx(0.2928932188));
}

// COLORS
TEST_CASE("create_color_black", "[create_color_black]")
{
	Color black{0.0};
	REQUIRE(black.r == 0);
	REQUIRE(black.g == 0);
	REQUIRE(black.b == 0);
}

TEST_CASE("create_color_red", "[create_color_red]")
{
	Color red{1.0,0.0,0.0};
	REQUIRE(red.r == 1);
	REQUIRE(red.g == 0);
	REQUIRE(red.b == 0);
}

// CIRCLE
TEST_CASE("create_circle_default", "[create_circle_default]")
{
	Circle c;
	REQUIRE(c.location().x() == 0);
	REQUIRE(c.location().y() == 0);
	REQUIRE(c.radius() == 0);
	REQUIRE(c.color().r == 0);
	REQUIRE(c.color().g == 0);
	REQUIRE(c.color().b == 0);
}

TEST_CASE("create_circle_userdefined", "[create_circle_userdefined]")
{
	Point2d p{2.0,2.0};
	p.translate(1.2,2.97);
	Circle c{p,3.0,Color{1.0,0.0,0.0}};
	REQUIRE(c.location().x() == Approx(3.2));
	REQUIRE(c.location().y() == Approx(4.97));
	REQUIRE(c.radius() == 3);
	REQUIRE(c.color().r == 1);
	REQUIRE(c.color().g == 0);
	REQUIRE(c.color().b == 0);
	REQUIRE(c.circumference() == Approx(2 * M_PI * 3));
}

// RECTANGLE
TEST_CASE("create_rectangle_default", "[create_rectangle_default]")
{
	Rectangle r;
	REQUIRE(r.location().x() == 0);
	REQUIRE(r.location().y() == 0);
	REQUIRE(r.width() == 0);
	REQUIRE(r.height() == 0);
	REQUIRE(r.color().r == 0);
	REQUIRE(r.color().g == 0);
	REQUIRE(r.color().b == 0);
}

TEST_CASE("create_rectangle_userdefined", "[create_rectangle_userdefined]")
{
	Rectangle r{Point2d{-1.22,-0.057},0.5,0.5,Color{0,0.5,0}};
	REQUIRE(r.location().x() == -1.22);
	REQUIRE(r.location().y() == -0.057);
	REQUIRE(r.width() == 0.5);
	REQUIRE(r.height() == 0.5);
	REQUIRE(r.color().r == 0);
	REQUIRE(r.color().g == 0.5);
	REQUIRE(r.color().b == 0);
	REQUIRE(r.circumference() == 2);
}

// IS_INSIDE
TEST_CASE("is_inside_circle", "[is_inside_circle]")
{
	Circle c{Point2d{0.5,0.5},0.25,Color{0}};
	Point2d p1{0.5,0.5};
	Point2d p2{0.5,0.26};
	Point2d p3{0.5,0.24};
	Point2d p4{0.1,0.1};
	REQUIRE(c.is_inside(p1) == true);
	REQUIRE(c.is_inside(p2) == true);
	REQUIRE(c.is_inside(p3) == false);
	REQUIRE(c.is_inside(p3) == false);
}

TEST_CASE("is_inside_rectangle", "[is_inside_rectangle]")
{
	Rectangle r{Point2d{0.4,0.4},0.2,0.2,Color{0}};
	REQUIRE(r.is_inside(Point2d{0.3,0.5}) == false);
	REQUIRE(r.is_inside(Point2d{0.7,0.5}) == false);
	REQUIRE(r.is_inside(Point2d{0.5,0.3}) == false);
	REQUIRE(r.is_inside(Point2d{0.5,0.7}) == false);
	REQUIRE(r.is_inside(Point2d{0.5,0.5}) == true);
}

TEST_CASE("comparison_operator_point2d", "[comparison_operator_point2d]")
{
	Point2d p1;
	Point2d p2{1,5};
	p1.translate(1,5);
	REQUIRE(p1 == p2);
}

int main(int argc, char *argv[])
{
  return Catch::Session().run(argc, argv);
}
